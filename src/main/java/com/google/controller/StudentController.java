package com.google.controller;

import com.google.pojo.StudentLogin;
import com.google.service.StudentLoginService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.NestedServletException;

/**
 * Created by dell on 2017/9/6.
 */
@Controller
public class StudentController {

    private final Log logger= LogFactory.getLog(StudentController.class);

    @Autowired
    StudentLoginService studentLoginService;

    @RequestMapping(value = "/studentlogin" ,method= RequestMethod.POST)
    public ModelAndView login(@RequestParam("studentid") String studentid, @RequestParam("studentpassword") String studentpassword, ModelAndView mv){
        StudentLogin studentLogin=new StudentLogin();
        try{
            System.out.println(studentid);
            studentLogin=studentLoginService.GetStudentById(Integer.parseInt(studentid));
            if(studentpassword.equals(studentLogin.getStudentPassword())){
                mv.addObject("message","success");
            }else{
                mv.addObject("message","faild");
            }
        }catch (NullPointerException e){
            mv.addObject("message","error");
            e.printStackTrace();
        }catch (Exception e){
            mv.addObject("message","not fonund");
            e.printStackTrace();
        }
        mv.setViewName("/WEB-INF/content/welcome");
        return mv;
    }
}
