package com.google.dao;

import com.google.pojo.StudentLogin;

public interface StudentLoginMapper {
    int deleteByPrimaryKey(Integer studentId);

    int insert(StudentLogin record);

    int insertSelective(StudentLogin record);

    StudentLogin selectByPrimaryKey(Integer studentId);

    int updateByPrimaryKeySelective(StudentLogin record);

    int updateByPrimaryKey(StudentLogin record);
}