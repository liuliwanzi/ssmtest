package com.google.service;

import com.google.pojo.StudentLogin;

/**
 * Created by dell on 2017/9/5.
 */
public interface StudentLoginService {
    public StudentLogin GetStudentById(Integer studentId);

}
