package com.google.service;


import com.google.dao.StudentLoginMapper;
import com.google.pojo.StudentLogin;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by dell on 2017/9/5.
 */
@Service
public class StudentLoginServiceImpl implements StudentLoginService {

    //日志对象
    private static final Log logger=  LogFactory.getLog(StudentLoginServiceImpl.class);

    @Autowired
    private StudentLoginMapper studentLoginMapper;

    @Override
    public StudentLogin GetStudentById(Integer studentId) {
        return this.studentLoginMapper.selectByPrimaryKey(studentId);
    }

}
